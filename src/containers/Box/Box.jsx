import React from 'react';
import 'containers/Box/style.sass'

const Box = (props) => {
  let { className } = props;
  let classNames = ['box', className].join(' ')
  return (
    <div className={classNames}>
      { props.children }
    </div>
  );
};

export default Box;