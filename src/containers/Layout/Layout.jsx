import React from 'react';
import Navigation from 'components/Navigation/Navigation'

import 'containers/Layout/style.sass'

const Layout = ({children}) => {
  return (
    <main className='layout'>
      <div className='container'>
        <Navigation />
        <section>
          { children }
        </section>
      </div>
    </main>
  );
};

export default Layout;