import axios from 'axios';

const SERVICES_URL = 'https://api.spacexdata.com/v3';

const api = axios.create();

export default {
  get: endpoint => api.get(`${SERVICES_URL}/${endpoint}`)
};