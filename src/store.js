import { createStore, combineReducers } from 'redux';
import launches from 'reducers/launches';

const createReducer = () => {
  const reducersToCombine = {
    // api
    launches,
  };
  return combineReducers(reducersToCombine);
}

const store = createStore(
  createReducer(),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;