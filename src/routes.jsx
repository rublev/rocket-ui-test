import React from 'react';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import Launches from 'views/Launches/Launches';
import Layout from 'containers/Layout/Layout'

const Routes = () => (
  <Router>
    <Layout>
      <Route exact path="/" component={Launches}/>
      <Route path="/launches" component={Launches}/>
    </Layout>
  </Router>
);

export default Routes;
