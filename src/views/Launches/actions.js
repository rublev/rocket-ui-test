import apiService from 'services/api';
import store from 'store';

/*
  i use the START/END convention here not REQUEST/RESPONSE because there are no api requests happening,
  instead it is meant to signify the start/end of the request/response cycle it embeds (for launches and rockets)
*/
import {
  FETCH_DATA_START,
  FETCH_DATA_END,
  SET_COST_DATA,
} from 'types'

export const fetchDataStart = () => ({
  type: FETCH_DATA_START
});

export const fetchDataEnd = payload  => ({
  type: FETCH_DATA_END,
  payload: {
    launches: payload
  }
});

export const setCostData = payload  => ({
  type: SET_COST_DATA,
  payload: {
    costs: payload
  }
});

export const fetchData = async (dispatch) => {

  dispatch(fetchDataStart())

  const launch = await apiService.get('launches').then(response => response.data);
  const rockets = await apiService.get('rockets').then(response => response.data);

  const launches = launch.map(launch => {
    const { id, cost_per_launch, description } = rockets.find(r => r.rocket_id === launch.rocket.rocket_id)
    return {
      mission: launch.mission_name,
      date_utc: launch.launch_date_utc,
      launch_success: launch.launch_success,
      rocket: {
        id,
        cost_per_launch,
        description,
      }
    }
  })

  dispatch(fetchDataEnd(launches))

};

export const getCostData = async (dispatch) => {
  const state = store.getState();
  const costs = [];
  state.launches.launches.map(launch => {
    costs.push(launch.rocket.cost_per_launch)
  })
  dispatch(setCostData(costs))
};