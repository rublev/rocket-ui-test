import React, { Component } from 'react';
import { connect } from 'react-redux';

import { fetchData, getCostData } from "views/Launches/actions";
import ExpandBox from 'components/ExpandBox/ExpandBox'
import Box from 'containers/Box/Box'
import RecentMission from 'components/RecentMission/RecentMission'

import 'views/Launches/style.sass'

import reddit from 'assets/icons/reddit.svg';
import community from 'assets/icons/community.svg';
import wikipedia from 'assets/icons/wikipedia.svg';
import chevron from 'assets/icons/chevron.svg';

const mapStateToProps = state => state;

const mapDispatchToProps = dispatch => ({
  dispatch
});

class LaunchesView extends Component {

  async componentDidMount() {
    const { dispatch, state } = this.props;
    await fetchData(dispatch);
    await getCostData(dispatch);
  }

  render() {
    const { launches } = this.props
    const { costs } = launches;
    const isLaunchesLoading = launches.fetching && launches.launches.length <= 0 && costs.length <= 9
    return (
      <div className='launches'>
        {isLaunchesLoading && <div>
          LOADING  
        </div>}
        {!isLaunchesLoading && 
          <div className='container'>
            <RecentMission
              costs={costs}
              recent={launches.launches[0].mission}
              recentCost={launches.launches[0].rocket.cost_per_launch}
            />
            <Box className='links'>
              <h3 className='medium'>Links</h3>
              <h4 className='blurb'>Information about SpaceX and other useful links.</h4>
              <hr />
              <div className='links'>
                <a className='link' target='_blank' href='https://en.wikipedia.org/wiki/SpaceX'>
                  <div className='icon-container'>
                    <img className='icon' src={wikipedia} />
                  </div>
                  <h4 className='medium'>Wikipedia</h4>
                  <img className='icon' src={chevron} />
                </a>
                <a className='link' target='_blank' href='https://forum.nasaspaceflight.com/index.php?board=45.0'>
                  <div className='icon-container'>
                    <img className='icon' src={community} />
                  </div>
                  <h4 className='medium'>Community</h4>
                <img className='icon' src={chevron} />
                </a>
                <a className='link' target='_blank' href='https://old.reddit.com/r/spacex/'>
                  <div className='icon-container'>
                    <img className='icon' src={reddit} />
                  </div>
                  <h4 className='medium'>Reddit</h4>
                <img className='icon' src={chevron} />
                </a>
              </div>
            </Box>
            <div className='missions'>
              <h2>Past Missions</h2>
              <ExpandBox
                launches={launches.launches}
              ></ExpandBox>
            </div>
          </div>
        }
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LaunchesView)