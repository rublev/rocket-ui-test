import {
  FETCH_DATA_START,
  FETCH_DATA_END,
  SET_COST_DATA,
} from 'types'

const initialState = {
  launches: [],
  costs: [],
  fetching: true
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case FETCH_DATA_START: {
      return {
        ...state,
        fetching: true
      }
    }
    case FETCH_DATA_END: {
      return {
        ...state,
        fetching: false,
        launches: [...state.launches, ...payload.launches]
      }
    }
    case SET_COST_DATA: {
      return {
        ...state,
        costs: payload.costs,
      }
    }
    default:
      return state;
  }
}