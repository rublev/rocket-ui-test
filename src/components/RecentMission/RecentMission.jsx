import React, { Component } from 'react';
import Box from 'containers/Box/Box'
import Chart from "chart.js";

import 'components/RecentMission/style.sass'

class RecentMission extends Component {
  constructor(props) {
    super(props)
  }
  componentDidUpdate(prevState) {
    if (prevState.costs.length !== this.props.costs.length) {
      this.renderChart(this.props.costs)
    }
  }

  renderChart(costs) {

    // set legend color to custom one because ctx gradient overlays legend when its position is set to 'bottom'
    Chart.plugins.register({
      beforeDraw: function (c) {
        var legends = c.legend.legendItems;
        legends.forEach(function (e) {
          e.fillStyle = '#a9e4d6';
        });
      }
    });

    const ctx = document.getElementById("myChart").getContext("2d");

    const gradient = ctx.createLinearGradient(0, 0, 0, 250);
    gradient.addColorStop(0, 'rgba(28,196,156,1)');
    gradient.addColorStop(1, 'rgba(28,196,156,0)');

    ctx.height = 269;

    const data = {
      type: 'line',
      data: {
        datasets: [
          {
            backgroundColor: gradient,
            data: costs,
            label: 'Logarithmic Cost Per Launch Over Time' // log because all the test values make an unreadable linear graph
          }
        ],
      },
      options: {
        tooltips: {
          enabled: false
        },
        responsive: true,
        maintainAspectRatio: false,
        legend: {
          position: 'bottom',
        },
        scales: {
          xAxes: [{
            display: false,
            gridLines: {
              display: false,
            }
          }],
          yAxes: [{
            ticks: {
              autoSkip: true,
              maxTicksLimit: 2
            },
            position: 'right',
            type: 'logarithmic',
            gridLines: {
              display: false,
            },
            ticks: {
              callback: function (value, index, values) {
                const format = Math.abs(value) > 999 ? Math.sign(value) * ((Math.abs(value) / 1000).toFixed(1)) + 'k' : Math.sign(value) * Math.abs(value)
                return index % 10 === 0 ? `$${format}` : null;
              }
            }
          }]
        },
      }
    };
    new Chart(ctx, data)
  }
  render() {
    const {recent, recentCost} = this.props
    const recentCostFormatted = recentCost.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    return (
      <Box className='recent-mission'>
        <div className='details'>
          <h1>{recent}</h1>
          <h2>Is the most recent mission.</h2>
          <h2>It cost <span className='money'>{`$${recentCostFormatted}`}</span> to launch.</h2>
        </div>
        <canvas id="myChart" />
      </Box>
    )
  }
}

export default RecentMission;