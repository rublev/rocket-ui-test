import React, { Component } from 'react';

import 'components/ExpandBox/style.sass'

import expand from 'assets/icons/expand.svg';
import close from 'assets/icons/close.svg';

class ExpandBox extends Component {
  
  constructor(props) {
    super(props);
    this.state = { selected: null };
  }

  onClick(mission) {
    this.setState({
      selected: mission !== this.state.selected ? mission : null
    })
  }

  isActive(mission) {
    if (this.selected === mission) {
      return 'active'
    } else {
      return 'hidden'
    }
  }

  render() {

    const { launches } = this.props

    return (
      <div className='expandbox'>
        {launches.map(({ mission, rocket }) => {
          const costPerLaunchFormatted = rocket.cost_per_launch.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
          let classNames = ['container', this.state.selected === mission ? "highlight" : ""].join(' ')
          let rocketClassNames = ['rocket', this.state.selected === mission ? "active" : "hidden"].join(' ')
          return (
            <div className={classNames} onClick={ () => this.onClick(mission) } key={mission}>
              <div className='visible'>
                <h1>{mission}</h1>
                {(this.state.selected !== mission) && <img className='icon expand' src={expand} />}
                {(this.state.selected === mission) && <img className='icon close' src={close} />}
              </div>
              <div key={mission} className={rocketClassNames}>
                <div className='row'>
                  <div className='label'>Rocket ID</div>
                  <div className='data'>{rocket.id}</div>
                </div>
                <div className='row'>
                  <div className='label'>Cost Per Launch</div>
                  <div className='data'>{`$${costPerLaunchFormatted}`}</div>
                </div>
                <div className='row'>
                  <div className='label'>Description</div>
                  <div className='data'>{rocket.description}</div>
                </div>
              </div>
            </div>
          )
        })}
      </div>
    );
  }
}

export default ExpandBox;