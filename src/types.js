// data
export const FETCH_DATA_START = 'FETCH_DATA_START';
export const FETCH_DATA_END   = 'FETCH_DATA_END';

// costs
export const SET_COST_DATA = 'SET_COST_DATA';